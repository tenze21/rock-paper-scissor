package main

import (
	"fmt"
	"log"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "<h1>Hello World</h1>")
	html := `<em>Hello world</em>`
	w.Header().Set("content-type", "text/html")
	fmt.Fprintf(w, html)
}
func main() {
	var port = 8080
	http.HandleFunc("/", homePage)

	http.ListenAndServe(":8080", nil)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println("server error", err)
		return
	}
	log.Println("starting server on port", port)
}
